package com.example.asdeksi;

import androidx.annotation.NonNull;

/**
 * Luokka josta luodaan olioita.
 * Johon tallennetaan askelia päivämäärän mukaan.
 */
public class StepsByDate {

    private String date;
    private int steps;

    /**
     * Konstruktori jossa luotavalle oliolle asetetaan päivämäärä ja askelet.
     * @param date päivämäärä
     * @param steps askeleet
     */
    public StepsByDate(String date, int steps) {
        this.date = date;
        this.steps = steps;
    }


    /**
     * Palauttaa olio-muuttujan date (päivämäärä) arvon.
     *
     * @return String date
     */
    public String getDate() {
        return date;
    }

    /**
     * Palauttaa olio-muuttuja steps (askeleet) arvon.
     *
     * @return int steps
     */
    public int getSteps() {
        return steps;
    }

    /**
     * Palauttaa olio-muuttuja date arvon.
     *
     * @return String date
     */
    @NonNull    // ei voi vastaanottaa null arvoa
    @Override
    public String toString() {
        return date;
    }
}

