
package com.example.asdeksi;

import java.lang.Math;
import java.util.ArrayList;

/**
 * BMI-Laskuri Singleton, joka laskee painoindeksin painosta ja pituudesta.
 * Sisältää merkkijono listan, kaksi int muuttujaa (sentit, kgrammat), ja kolme
 * double muuttujaa (metrit, lyhytBmi & tarkkaBmi).
 * @author JuliaKoykka
 */

public class bmiLaskuri {
    static bmiLaskuri single = new bmiLaskuri();
    private ArrayList<String> kategoriat = new ArrayList<>(); //kategoriat
    private int sentit, kilot;
    private double metrit, lyhytBmi, tarkkaBmi;

    /**
     * Singleton konstruktori ja LÄHDE kategorisoinnin tiedoille
     * https://www.painoindeksi.org/uusi/laske.php
     */
    private bmiLaskuri() {
        //
        makeList();
    }

    /**
     * Palauttaa singletonin.
     * @return bmiLaskuri
     */
    public static bmiLaskuri getInstance(){
        return single;
    }

    /**
     * Lisää kategoriat listaan
     */
    public void makeList(){
        kategoriat.add("Vaikea alipaino : alle 16.0");
        kategoriat.add("Merkittävä alipaino : 16.0 - < 17");
        kategoriat.add("Lievä alipaino : 17.0 - < 18.5");
        kategoriat.add("Normaali paino : 18.5 - < 25");
        kategoriat.add("Lievä ylipaino : 25.0 - < 30");
        kategoriat.add("Merkittävä ylipaino : 30.0 - < 35");
        kategoriat.add("Vaikea ylipaino : 35.0 - < 40");
        kategoriat.add("Sairaalloinen lihavuus : >= 40");
    }

    /**
     * Vie painokategoriat eteenpäin
     * @return ArrayList    näytettävä kategorioiden lista
     */
    public ArrayList<String> getKategoriat(){
        return kategoriat;
    }

    /**
     * Rekisteröidään uudet arvot pituus, paino -ja bmi muuttujiin, sekä
     * muutetaan shortBmi yhden desimaalin tarkkuuteen.
     * @param sentit    pituus senttimetreinä
     * @param kilot  paino kilogrammoina
     */
    public void setup(int sentit, int kilot){
        this.sentit = sentit; this.kilot = kilot;
        metrit = sentit * 0.01;
        tarkkaBmi = (1.3 * kilot) / (Math.pow(metrit,2.5));
        //tehdaan pitkästä liukuluvusta yhden desimaalin tarkkuinen
        int skaala = (int) Math.pow(10, 1);
        lyhytBmi = (double) Math.round(tarkkaBmi * 10) / 10;
    }

    /**
     * Kertoo mihin kategoriaan syötetty paino kuuluu
     * @return String
     */
    public String getTier(double bmi){
        if (bmi < 16.0) {
            return "Vaikea alipaino";
        } else if (bmi >= 16.0 && bmi < 17.0) {
            return "Merkittävä alipaino";
        } else if (bmi >= 17.0 && bmi < 18.5) {
            return "Lievä alipaino";
        } else if (bmi >= 18.5 && bmi < 25.0) {
            return "Normaali paino";
        } else if (bmi >= 25.0 && bmi < 30.0) {
            return "Lievä ylipaino";
        } else if (bmi >= 30.0 && bmi < 35.0) {
            return "Merkittävä ylipaino";
        } else if (bmi >= 35.0 && bmi < 40.0) {
            return "Vaikea ylipaino";
        } else if (bmi >= 40.0) {
            return "Sairaalloinen lihavuus";
        }
        return "Jokin meni pieleen..";
    }

    /**
     * Hakee bmi:n yhden desimaalintarkkuudella merkkijonoksi.
     * @return String
     */
    public String getBmi(){
        return Double.toString(lyhytBmi);
    }

    /**
     * Merkkijono, johon sisältyy pituus (cm), paino (kg), BMI:n tulos, sekä painonkategoria.
     * @return String
     */
    @Override
    public String toString() {
        return "Pituus : " + sentit + " cm\nPaino : " + kilot + " kg\nPainoindeksi (BMI) : "
                + lyhytBmi + "\n" + getTier(lyhytBmi);
    }
}
