package com.example.asdeksi;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;

/**
 * BMR-laskurin aktiviteetti. Laskurilla ei ole erillistä koodia.
 * @author JuliaKöykkä
 */
public class bmrActivity extends AppCompatActivity {
    private bmrLaskuri laskuri;
    private Switch spKytkin;
    private Boolean mies;
    private EditText pituusEdit, painoEdit, ikaEdit;
    private TextView tulos;
    private Button sendBut;
    private int sentit, kilot, vuodet;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bmr);
        laskuri = bmrLaskuri.getInstance();
        pituusEdit = findViewById(R.id.pituusEt);
        painoEdit = findViewById(R.id.painoEt);
        ikaEdit = findViewById(R.id.ikaEt);
        sendBut = findViewById(R.id.sendBut);
        sendBut.setOnClickListener(kuuntelija);
        spKytkin = findViewById(R.id.spSw);
        tulos = findViewById(R.id.tulosTv);
    }

    /**
     * OnClick asettaa pituudelle, painolle ja iälle arvot, sekä laskee laskurilla bmr:n ja
     * asettaa tämän tiedot tulostekstiin
     */
    private View.OnClickListener kuuntelija = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (v.getId() == R.id.sendBut) {
                //nappula ei toimi jos kentät on tyhjiä
                if (!pituusEdit.getText().toString().isEmpty() || !painoEdit.getText().toString().isEmpty() || !ikaEdit.getText().toString().isEmpty()) {
                    //ottaa arvot EditText palkeista muuttujiin
                    sentit = Integer.parseInt(pituusEdit.getText().toString());
                    kilot = Integer.parseInt(painoEdit.getText().toString());
                    vuodet = Integer.parseInt(ikaEdit.getText().toString());
                    laskuri.setup(sentit, kilot, vuodet);
                    if (spKytkin.isChecked()){
                        laskuri.laskeMies();
                    } else {
                        laskuri.laskeNainen();
                    }
                    tulos.setText(laskuri.getString());
                }

            }
        }
    };
}