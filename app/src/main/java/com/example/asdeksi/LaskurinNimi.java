package com.example.asdeksi;

import android.content.SharedPreferences;

public class LaskurinNimi {
    private String name;

    public LaskurinNimi(String name){
        this.name = name;
    }

    public String getName(){
        return this.name;
    }
    @Override
    public String toString(){
        return this.name;
    }
}
