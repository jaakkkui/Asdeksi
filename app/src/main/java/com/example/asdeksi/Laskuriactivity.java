package com.example.asdeksi;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

public class Laskuriactivity extends AppCompatActivity {

    private TextView tvSteps;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_laskuriactivity);

        Bundle b = getIntent().getExtras();
        int ind = b.getInt("laskuriIndeksi");

        TextView tv = findViewById(R.id.timeButton);
        tvSteps = findViewById(R.id.tvTime);

        ListStuff listat = ListStuff.getInstance();
        LaskurinNimi  l = listat.getLaskurinNimi(ind);
        tv.setText(l.getName());


    }

}