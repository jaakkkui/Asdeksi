package com.example.asdeksi;

/**
 * BMR-Laskuri Singleton, joka laskee painoindeksin painosta ja pituudesta.
 * Sisältää neljä int muuttujaa (sentit, kilot, vuodet, kalorit) ja
 * double muuttujan (luku).
 * @author JuliaKoykka
 */
public class bmrLaskuri {
    static bmrLaskuri single = new bmrLaskuri();
    private int sentit, kilot, vuodet, kalorit;
    private double luku;

    private bmrLaskuri() {
    }

    public static bmrLaskuri getInstance(){
        return single;
    }

    /**
     * Rekisteröidään uudet arvot pituus, paino -ja ikä muuttujiin.
     * @param sentit    pituus (cm)
     * @param kilot     paino (kg)
     * @param vuodet    ikä (vuosi)
     */
    public void setup(int sentit, int kilot, int vuodet){
        this.sentit = sentit;
        this.kilot = kilot;
        this.vuodet = vuodet;
    }

    /**
     *Laskee miehen BMR
     */
    public void laskeMies(){
        //Men: (10 × weight in kg) + (6.25 × height in cm) - (5 × age in years) + 5
        luku = ((10*kilot)+(6.25*sentit)-(5*vuodet)+5);
    }

    /**
     *Laskee naisen BMR
     */
    public void laskeNainen(){
        //Women: (10 × weight in kg) + (6.25 × height in cm) - (5 × age in years) - 161
        luku = ((10*kilot)+(6.25*sentit)-(5*vuodet)-161);
    }

    /**
     * Antaa tuloksen merkkijonona
     * @return String   levossa kulutettujen kalorien määrä
     */
    public String getString(){
        kalorit = (int) luku;
        return kalorit + " kilokaloria";
    }
}
