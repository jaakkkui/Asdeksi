package com.example.asdeksi;

import android.content.SharedPreferences;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

/**
 * Luokka jolla tallennetaan ja hallitaan askelia.
 * Avain määritelty public static final String muuttujana, jota kutsuvan luokan tulee käyttää luodessaan SharedPreferences oliota.
 */
public class StepsSaver {

    public static final String STEPS_PREFERENCES = "AsdeksiPreferences";
    public static final String STEPS_KEY = "StepsKey"; // avain kokonaisAskelille
    public static final String STEPS_TODAY ="StepsToday";

    /**
     * Haetaan kaikkien tallennettujen askelien määrä.
     *
     * @param sharedPreferences SharedPreferences
     * @return int askeleet
     */
    public static int getSteps(SharedPreferences sharedPreferences) {
        return sharedPreferences.getInt(STEPS_KEY, 0);
    }

    /**
     * Hakee kaikki tämän päivän askeleet.
     * @param sharedPreferences SharedPreferences
     * @return int päivän askeleet
     */
    public static int getStepsToday(SharedPreferences sharedPreferences) {
        return sharedPreferences.getInt(STEPS_TODAY, 0);
    }

    /**
     * Hakee tietylle päivämäärälle tallennettujen askelten määrän.
     *
     * @param sharedPreferences SharedPreferences
     * @param date String, päivämäärä jonka askeleet halutaan palauttaa
     * @return int askeleet
     */
    public static int getStepsByDate(SharedPreferences sharedPreferences, String date) {
        return sharedPreferences.getInt(date, 0);
    }

    /**
     * Poistaa kaikki tallennetut askeleet
     * @param sharedPreferences SharedPreferences
     */
    public static void resetSteps(SharedPreferences sharedPreferences) {
        SharedPreferences.Editor sharedPreferencesEditor = sharedPreferences.edit();

        sharedPreferencesEditor.clear();
        sharedPreferencesEditor.putInt(STEPS_KEY, 0);
        sharedPreferencesEditor.commit();
    }

    /**
     * Kasvattaa kokonaisaskelia sekä päivän askelia yhdellä.
     * @param sharedPreferences SharedPreferences
     */
    public static void addSteps(SharedPreferences sharedPreferences) {
        SharedPreferences.Editor sharedPreferencesEditor = sharedPreferences.edit();

        Calendar calendar = Calendar.getInstance();
        String currentDate = DateFormat.getDateInstance().format(calendar.getTime());

        int currentSteps = getSteps(sharedPreferences);
        int stepsByDate = getStepsByDate(sharedPreferences, currentDate);

        sharedPreferencesEditor.putInt(STEPS_KEY, currentSteps + 1);
        sharedPreferencesEditor.putInt(currentDate, stepsByDate + 1);
        sharedPreferencesEditor.putInt(STEPS_TODAY, stepsByDate +1);
        sharedPreferencesEditor.commit();
    }

    /**
     * Palauttaa askeleet per päivämäärä-olio(StepsByDate) listan.
     * @param sharedPreferences SharedPreferences
     * @return Lista tallennetuista päivämääristä, jonka sisällä on askeleet
     */
    public static List<StepsByDate> getAllSteps(SharedPreferences sharedPreferences) {
        List<StepsByDate> list = new ArrayList<>();

        for (Map.Entry < String, Integer> entry : ((Map<String, Integer>) sharedPreferences.getAll()).entrySet()) {
            String date = entry.getKey();
            int steps = entry.getValue();

            if(!date.equals(STEPS_KEY) && !date.equals(STEPS_TODAY)) {
                list.add(new StepsByDate(date, steps));
            }


        }
        return list;
    }
}
