package com.example.asdeksi;

import java.util.ArrayList;

/**
 * Aloitus näkymän ListView-luokka. Asettaa muut aktiviteetit listaan.
 */
public class ListStuff {
    private static final  ListStuff listStuff = new ListStuff();

    /**
     * Palauttaa listasta esiintymän
     *
     * @return ListStuff    aloitusnäkymän listan instanssi
     */
    public static ListStuff getInstance(){
        return listStuff;
    }

    private final ArrayList<String> aktiviteetit;

    private ListStuff(){
        aktiviteetit = new ArrayList<>();
        getLista().add("BMI");
        getLista().add("Askeleet");
        getLista().add("BMR");
        getLista().add("Info");
    }

    /**
     * Hakee listan kokonaisuudessaan
     * @return List
     */
    public ArrayList<String> getLista(){
        return this.aktiviteetit;
    }

    /**
     * Hakee indeksin määrittelemän olion listasta
     * @param index merkkijonon sijainti listassa
     * @return String
     */
    public String getIndex(int index){
        return  this.aktiviteetit.get(index);
    }
}
