
package com.example.asdeksi;

import androidx.appcompat.app.AppCompatActivity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

/**
 * Painoindeksin aktiviteetti, joka asettaa käyttöliittymän toiminallisuutta.
 * Sisältää BMI laskurin, kaksi EditText-widgettiä, TextView widgetin, painikkeen,
 * listanäkymän, adapterin ja preferenssi asetukset.
 * @author JuliaKoykka
 */

public class bmiActivity extends AppCompatActivity {

    private bmiLaskuri laskuri;
    private EditText pituusTeksti, painoTeksti;
    private TextView bmiTeksti;
    private Button bmiBut;
    private ListView kategoriaView;
    private ArrayAdapter<String> adapteri;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bmi);
        laskuri = bmiLaskuri.getInstance();
        pituusTeksti = findViewById(R.id.pitText);
        painoTeksti = findViewById(R.id.paiText);
        bmiTeksti = findViewById(R.id.bmiTV);
        kategoriaView = findViewById(R.id.katList);
        bmiBut = findViewById(R.id.bmiBut);
        bmiBut.setOnClickListener(kuuntelija);
        // Adapteri hakee laskurista kategoriat ja sijoittaa ne ListView:hin
        adapteri = new ArrayAdapter<>(this,R.layout.rivileiska, laskuri.getKategoriat());
    }

    /**
     * OnClick asettaa pituudelle ja painolle arvot, täyttää ListView widgetin kategorioilla kun
     * painiketta on painettu, sekä laskee laskurilla bmi:n ja asettaa tämän tiedot tulostekstiin
     */
    private View.OnClickListener kuuntelija = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (v.getId() == R.id.bmiBut) {
                //nappula ei toimi jos kentät on tyhjiä
                if (!pituusTeksti.getText().toString().isEmpty() || !painoTeksti.getText().toString().isEmpty()) {
                    //ottaa arvot EditText palkeista muuttujiin
                    int sentit = Integer.parseInt(pituusTeksti.getText().toString());
                    int kgrammat = Integer.parseInt(painoTeksti.getText().toString());
                    //asettaa laskurin ja laskee bmi
                    laskuri.setup(sentit, kgrammat);
                    //kategoria listan asetus
                    kategoriaView.setAdapter(adapteri);
                    kategoriaView.setVisibility(View.VISIBLE);
                    //bmi tulos tekstipalkkiin
                    bmiTeksti.setText(laskuri.toString());
                }
            }
        }
    };
}