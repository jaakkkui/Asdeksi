package com.example.asdeksi;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

/**
 * Activity joka käynnistyy kun PreviousStepsListActivityssä tehdään valinta
 */
public class PreviousStepsActivity extends AppCompatActivity {

    /**
     * Vaihtaa kahden tekstinäkymän tekstit Intentin extroissa saatujen tietojen mukaisiksi.
     * Eli vaihtaa arvot päivämääräksi ja askeleiksi
     * @param savedInstanceState Bundle
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_previous_steps);

        Bundle bundle = getIntent().getExtras();
        String date = bundle.getString(PreviousStepsListActivity.STEP_HISTORY_LIST_MESSAGE_DATE);
        int steps = bundle.getInt(PreviousStepsListActivity.STEP_HISTORY_LIST_MESSAGE_STEPS);

        ((TextView) findViewById(R.id.tvHistoryDate)).setText(date);
        ((TextView) findViewById(R.id.tvHistorySteps)).setText(Integer.toString(steps));
    }
}