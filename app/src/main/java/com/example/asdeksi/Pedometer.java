package com.example.asdeksi;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;


//Mallia otettu  https://www.youtube.com/watch?v=7d6iKupzkEg ja https://www.geeksforgeeks.org/android-alert-dialog-box-and-how-to-create-it/
/**
 * Askelmittari aktivitieetti
 */
public class Pedometer extends AppCompatActivity {
    private static TextView tvSteps;    // Askeleet sovelluksen avattua. Nollaantuu aina kun sovelluksen sulkee
    private static TextView tvAllSteps; // Kaikki tämän päivän askeleet
    public Button plusStep;             // Lisäys nappi
    public Button resetStep;            // reset nappi
    public Button resetAll;             // nollaa kaikki myös historia
    public static int stepCounter;      // Askelien laskuri

    /**
     * Napeille +, reset ja reset all annetaan onClick toiminto.
     */
    public View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.plus:
                    plusStep();
                    break;

                case R.id.reset:
                    resetStep();
                    break;

                case R.id.resetAll:
                    resetHistory();
                    break;
            }
        }
    };

    /**
     * Asetetaan muuttujille oikeat arvot.
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pedometer);
        tvSteps = (TextView) findViewById(R.id.tvSteps);
        tvAllSteps = (TextView) findViewById(R.id.tvAllSteps);
        plusStep = (Button) findViewById(R.id.plus);
        plusStep.setOnClickListener(clickListener);
        resetStep = (Button) findViewById(R.id.reset);
        resetStep.setOnClickListener(clickListener);
        resetAll = (Button) findViewById(R.id.resetAll);
        resetAll.setOnClickListener(clickListener);

        stepCountStart();
    }

    /**
     * stepCounterille annetaan arvo nolla aina kun sovellus avataan uudelleen.
     * tvAllStepsille asetetaan arvoksi kaikki tämän aikana otetutut askeleet.
     */
    public void stepCountStart() {
        stepCounter = 0;
        tvSteps.setText(String.valueOf(stepCounter));
        // Asettaa tvAllSteps arvoksi kaikki tämän päivän aikana otetut askeleet
        tvAllSteps.setText(Integer.toString(StepsSaver.getStepsToday(getSharedPreferences(StepsSaver.STEPS_PREFERENCES, Activity.MODE_PRIVATE))));
    }

    /**
     * lisätään molempien textViewien arvoja yhdellä.
     */
    public void plusStep() {
        stepCounter++;
        StepsSaver.addSteps(getSharedPreferences(StepsSaver.STEPS_PREFERENCES, Activity.MODE_PRIVATE));
        tvSteps.setText(String.valueOf(stepCounter));
        // Asettaa tvAllSteps arvoksi kaikki tämän päivän aikana otetut askeleet
        tvAllSteps.setText(Integer.toString(StepsSaver.getStepsToday(getSharedPreferences(StepsSaver.STEPS_PREFERENCES, Activity.MODE_PRIVATE))));
    }

    /***
     * Reset nappi joka asettaa askelmittarin arvon nollaksi. Tarkistaa ensin käyttäjältä haluaako varmasti nollata arvon.
     * By.Jaakko
     */
    public void resetStep() {
        AlertDialog.Builder builder = new AlertDialog.Builder(Pedometer.this);
        builder.setMessage("Reset steps?");
        builder
                .setTitle("Reset")
                .setCancelable(true)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        stepCounter = 0;
                        tvSteps.setText(String.valueOf(stepCounter));
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    /**
     * Nappulaa RESET ALL painaessa kysyy haluatko varmasti poistaa kaikki tiedot.
     * Eli tämän päivän askeleet ja kaikki vanhojen päivien tiedot.
     */
    public void resetHistory() {
        AlertDialog.Builder builder = new AlertDialog.Builder(Pedometer.this);
        builder.setMessage("Do you really want to delete everything?");
        builder
                .setTitle("Reset")
                .setCancelable(true)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        stepCounter = 0;
                        StepsSaver.resetSteps(getSharedPreferences(StepsSaver.STEPS_PREFERENCES, Activity.MODE_PRIVATE));
                        tvSteps.setText(String.valueOf(stepCounter));
                        updateUI();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    /**
     * Nappulaa History painaessa siirrytään listaan missä näkyy kaikki aikaisemmat päivät jos on .
     * Niitä klikkaamalla näkyy myös niiden päivien saa
     * @param view View
     */
    public void historyButtonPressed(View view) {
        Intent PreviousStepsListActivity = new Intent(this, PreviousStepsListActivity.class);
        startActivity(PreviousStepsListActivity);
    }

    /**
     * Päivittää UI:n askelmäärä tekstielementit.
     */
    private void updateUI() {
        tvAllSteps.setText(Integer.toString(StepsSaver.getSteps(getSharedPreferences(StepsSaver.STEPS_PREFERENCES, Activity.MODE_PRIVATE))));
    }

}