package com.example.asdeksi;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.text.DateFormat;
import java.util.Date;
import java.util.List;

/**
 * Activity joka tulee history nappulaa painaessa pedometer activityssä.
 * Näytölle luodaan lista tallennetuista askelista päivämäärineen.
 */
public class PreviousStepsListActivity extends AppCompatActivity {

    public static final String STEP_HISTORY_LIST_MESSAGE_DATE = "com.example.asdeksi.DATE";
    public static final String STEP_HISTORY_LIST_MESSAGE_STEPS = "com.example.asdeksi.STEPS";

    /**
     * Luo näytölle listanäkymän askelia_per_päivämäärä(StepsByDate)-olioista.
     * @param savedInstanceState Bundle
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_previous_steps_list);

        final List<StepsByDate> stepsByDateArrayList = StepsSaver.getAllSteps(getSharedPreferences(StepsSaver.STEPS_PREFERENCES, Activity.MODE_PRIVATE));
        //stepsByDateArrayList.add(new StepsByDate("tt", 1));  // TESTI

        ListView listView = findViewById(R.id.stepsHistoryListView);
        listView.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, stepsByDateArrayList));

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Bundle bundle = new Bundle();
                bundle.putString(STEP_HISTORY_LIST_MESSAGE_DATE, stepsByDateArrayList.get(position).getDate());
                bundle.putInt(STEP_HISTORY_LIST_MESSAGE_STEPS, stepsByDateArrayList.get(position).getSteps());

                Intent PreviousStepsActivity = new Intent(PreviousStepsListActivity.this, PreviousStepsActivity.class);
                PreviousStepsActivity.putExtras(bundle);
                startActivity(PreviousStepsActivity);
            }
        });
    }
}