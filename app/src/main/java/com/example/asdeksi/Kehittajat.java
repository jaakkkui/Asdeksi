package com.example.asdeksi;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

/**
 * Kehittäjien tiedot aktiviteetissa
 */

public class Kehittajat extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kehittajat);
    }
}