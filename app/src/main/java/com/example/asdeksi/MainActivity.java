package com.example.asdeksi;

import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.text.DateFormat;
import java.util.Calendar;

/**
 * Aloitus näkymä, käyttöliittymä, ja sen toiminnallisuus.
 * Asettaa ListView, jossa painikkeet muihin aktiviteetteihin.
 */
public class MainActivity extends AppCompatActivity {

    private Calendar calendar;
    private TextView tvTime;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        tvTime = findViewById(R.id.tvTime);
        ListView lv = findViewById(R.id.lvLaskurit);
        ListStuff lista = ListStuff.getInstance();
        lv.setAdapter(new ArrayAdapter<>(this, R.layout.omatyyli, lista.getLista()));
        lv.setOnItemClickListener((parent, view, position, id) -> {
            switch(position){
                case 0:
                    Intent bmi = new Intent(MainActivity.this, bmiActivity.class);
                    startActivity(bmi);
                    break;
                case 1:
                    Intent askel = new Intent(MainActivity.this, Pedometer.class);
                    startActivity(askel);
                    break;
                case 2:
                    Intent lepovai = new Intent(MainActivity.this, bmrActivity.class);
                    startActivity(lepovai);
                    break;
                case 3:
                    Intent kehittajat = new Intent(MainActivity.this, Kehittajat.class);
                    startActivity(kehittajat);
                    break;

            }
        });
        calendar = Calendar.getInstance();
        updateUI();
    }

    /**
     * Metodilla voi siirtyä askelmittarin aktiviteettiin (ei ole käytössä!)
     *
     * @param view  näkymä parametri
     */
    public void toPedometer(View view){
        Intent intent = new Intent(this, Pedometer.class);
        startActivity(intent);
    }

    /**
     * Päivittää UI:n tvTime textview:tä
     */
    private void updateUI(){
        String currentDate = DateFormat.getDateInstance().format(calendar.getTime());
        tvTime.setText(currentDate);
    }
}
